<div id="news">
<b>Year-end meeting TOMORROW!</b><br>
<i>Wednesday, May 2, 2018</i><br>

Join us for the year-end meeting in Weber 201 at 12:00pm.<br>
We will vote on officers for next year and take a group photo- make sure to wear your SIAM shirt if you have one!
</div>


<div id="news">
<b>Project Euler TOMORROW!</b><br>
<i>Wednesday, April 23, 2018</i><br>

Please join us in Weber 237 from 4:00pm-6:00pm and be ready to work on some Project Euler problems!<br>
Pizza and ice cream will both be provided! Bring a laptop, brain, and enthusiasm!
</div>


