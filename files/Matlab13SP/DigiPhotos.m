
% Digital Photography Processing:  MATLAB Tutorial
% Justin Marks, October 26, 2011 
% SIAM Student Chapter at CSU

%Disclaimer: There is a separate Image Processing Toolbox with many
% additional functions for image manipulation.  While this toolbox is 
% very useful and interesting, this lesson will only
% exploit functions available with standard MATLAB.

%This lesson looks at a digital picture as a matrix, and performs a 
%variety of transformations for the image.

% %loading photos
% A = imread('JustinFace.jpg');
% [numrows,numcolumns,numcolors]=size(A)
% numpixels = numrows*numcolumns  %how many megapixels is this?
% pause
% 
% %displaying photos
% image(A)
% title('Original Image')
% pause
% 
% %eliminate the red sheet
% B = A;
% zerosheet = zeros(numrows,numcolumns);
% B(:,:,1) = zerosheet; 
% figure
% image(B)
% title('No Red');
% pause
% 
% %save the new image
% imwrite(B,'NoRed.jpg','jpg')
% pause
% 
% %convert to grayscale image
% G = .2989*A(:,:,1)+.5870*A(:,:,2)+.1140*A(:,:,3);
% figure
% colormap(gray(256));
% image(G)
% title('Grayscale')
% pause
% 
% %convert to only white (255) or black (0) pixels
% BW=G;
% for cutoff = 10:10:150
% for i=1:size(G,1)
%     for j = 1:size(G,2)
%         if G(i,j)>cutoff
%             BW(i,j) = 255;
%         else
%             BW(i,j) = 0;
%         end
%     end
% end
% colormap(gray(256))
% image(BW)
% title(['B&W with Cutoff=', int2str(cutoff)])
% pause
% end
% imwrite(BW,'BW.jpg','jpg')
% 
% %using SVD - a powerful tool for image analysis
% Geye=G(601:700,801:900); %smaller piece
% [a,b] = size(Geye)
% [U,S,V]=svd(double(Geye)); %we have that G=USV'
% 
% for i=2:2:b
%     approx = U(:,1:i)*S(1:i,1:i)*V(:,1:i)';  %best rank i rank approx
%     colormap(gray(256));
%     image(approx)
%     title(['Best Approx Image with rank=',int2str(i)])
%     pause
% end
% 
% %Vectorizing photos
% size(G)
% vecG = G(:);
% size(vecG)
% G2=reshape(vecG,numrows,numcolumns);
% size(G2)

%many face photos together
F = []; 
numimages=15;
for i = 1:numimages
    if i<10
        name=['regPAL0',int2str(i),'.tiff'];
    else
        name=['regPAL',int2str(i),'.tiff'];
    end
    newf=imread(name); %loads a grayscale face 800x1400 = about 1 MP 
    colormap(gray(256));
    image(newf)
    title(['Original image ', int2str(i)])
    vecf = newf(:);  %vectorize
    F=[F vecf]; %concatenate into matrix
    pause
end
F=double(F);  %F is 1120000 by numimages
[U,S,V] = svd(F,0);  %thin svd
format long
diag(S)

for j=1:8 %look at the first 8 Principal Faces
    vecPrincipalFace = U(:,j);
    PrincipalFace = reshape(vecPrincipalFace,800,1400);
    imagesc(PrincipalFace)
    title(['PrincipalFace ', int2str(j)])
    pause
end
        


