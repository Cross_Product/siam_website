%Example of a handle class, created for the SIAM workshop at CSU, 10/24/2011
%Cal = Calendar creates a new calendar object with default date
%Cal = Calendar(month,day,year) creates a new calendar object with given date
%Cal = Calendar(month,day,year,numyears) sets Cal.EndFlag = 1 after
%   numyears number of years have passed.  The last such year is stored in Cal.LastYear
%Other functionality included
%
%By Christopher Strickland, distribute freely

classdef Calendar < handle %One calendar is meant to be shared across the program
    properties (SetAccess = private) %Only object can set, everyone can view
        Year = 1;
        Month = 1;
        DayOfMonth = 1;
        DayOfYear = 1;
        Day = 1; %total days the calendar has counted
        FirstDayFlag = 1;
        %Year timer info
        NumberOfYears = [];
        FirstYear = 1;
        LastYear = [];
        EndFlag = 0;
        %
    end
    
    properties (Access = private) %Only calendar object can set and see
        %properties for inner working of the calendar
        DaysInMonth = [31;28;31;30;31;30;31;31;30;31;30;31];
        PrevYear = 1;
        PrevMonth = 0;
        PrevDayOfMonth = 0;
        PrevDayOfYear = 0;
        %
        %secret random date
        RandDay = ceil(rand(1)*365);
    end
    
    methods
        function Cal = Calendar(Month,DayOfMonth,Year,NumberOfYears) %Constructor
            if nargin > 0,
                Cal.Month = Month;
                Cal.DayOfMonth = DayOfMonth;
                Cal.Year = Year;
                Cal.DayOfYear = sum(Cal.DaysInMonth(1:Cal.Month-1))+Cal.DayOfMonth;
                Cal.FirstYear = Year;
            end
            if nargin > 3,
                Cal.NumberOfYears = NumberOfYears;
                Cal.LastYear = Cal.Year + NumberOfYears;
            end
        end
        
        function nextDay(Cal) %Advance calendar one day
            Cal.Day = Cal.Day + 1; %advance total count
            Cal.FirstDayFlag = 0; %turn off first day flag
            if Cal.DayOfYear == 365, %end of year check
                Cal.Year = Cal.Year + 1;
                Cal.PrevMonth = 12;
                Cal.Month = 1;
                Cal.PrevDayOfMonth = 31;
                Cal.DayOfMonth = 1;
                Cal.PrevDayOfYear = 365;
                Cal.DayOfYear = 1;
                if Cal.Year > Cal.NumberOfYears, %end of data check
                    Cal.EndFlag = 1;
                end
            elseif Cal.DayOfMonth == Cal.DaysInMonth(Cal.Month), %end of month check
                Cal.PrevMonth = Cal.Month;
                if Cal.PrevMonth == 1,
                    Cal.PrevYear = Cal.Year;
                end
                Cal.Month = Cal.Month + 1;
                Cal.PrevDayOfMonth = Cal.DayOfMonth;
                Cal.DayOfMonth = 1;
                Cal.PrevDayOfYear = Cal.DayOfYear;
                Cal.DayOfYear = Cal.DayOfYear + 1;
            else
                Cal.PrevDayOfMonth = Cal.DayOfMonth;
                Cal.DayOfMonth = Cal.DayOfMonth + 1;
                Cal.PrevDayOfYear = Cal.DayOfYear;
                Cal.DayOfYear = Cal.DayOfYear + 1;
            end
        end
        
        function resetFirstDayFlag(Cal) %Reset the first day flag
            Cal.FirstDayFlag = 1;
        end
        
        function resetYearTimer(Cal,NumberOfYears) %Reset year timer
            Cal.FirstYear = Cal.Year;
            Cal.LastYear = Cal.Year + NumberOfYears;
            Cal.NumberOfYears = NumberOfYears;
            Cal.EndFlag = 0;
        end
        
        function flag = isRandDay(Cal) %Ask the calendar if today is the secret random day
            if Cal.RandDay == Cal.DayOfYear,
                flag = 1;
            else
                flag = 0;
            end
        end
        
        function resetRandDay(Cal) %Reset the secret random day
            Cal.RandDay = ceil(rand(1)*365);
        end
        
%         function delete(Cal)  %deconstructor (handle only) - not needed.
%         end
    end
end