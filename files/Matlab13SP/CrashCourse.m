% Crash Course Lecture: Matlab Fundamentals
% 
% Matlab = Matrix Laboratory
% 
% Starting Matlab
% Windows 
% 	command window
% 	workspace
% 	command history
% 
% Matlab as a calculator
((1+2)*3)^4
sin(pi)
asin(1/sqrt(2))
sin(1)

%to display more or less decimal places
format short
format long
format short e
format long e
% Help
% Lots of information!
% >> help <command name>
%such as:  "help format"

% Special Functions and Values
pi
e=exp(1)
log(e)

% Using variables
% A variable is a means of storing numbers (information).
% 
a=2
b=3
c=a*b
d=a^b
e=cos(d)
% display the current variables
who
whos
% semicolon suppresses output
f=csc(pi/2);
f

clear  % removes all variable definitions

% Continue a line with three periods in succession
b=2
d=5
a = 3+b-...
d
% 
% ?(uparrow) and ?(downarrow) scroll backwards and forwards through command
% ? (forward arrow) and ? (backward) arrow move cursor along command line.
% 
% Return   with the cursor at any location executes command.
% Escape   erases command.
% Ctrl+C   stops calculation.  % Life Saver
% 
%clc	 % clears screen.
%quit %  exits Matlab.
% 

% M-files
%  In order to save a command sequence, 
%  you will want to create an M-file.  
%  From the �File� menu, click on �New� and �M-file.�  
%  This will open an editor window.  
%  You can type commands here and save the file with a .m extension.
%  You can execute your saved file at any time 
%  by typing the name of the file (without the .m) in the command window.  
%  You can also execute the file by using the F5 button 
%  or the icon that looks like a down arrow next to a page.

%Functions
%
%Syntax for writing functions:
%
%First line of m-file looks like
%
%function myfunction(a,b)	% two inputs, no outputs
%function a = myfunction(b)	% one input, one output
%function [a,b] = myfunction(c,d,e)	%three inputs, two outputs.
%
%Note: First line MUST start with function.
%
%Also, you MUST save the m-file with the same name as the function.  
%You can use at most 31 characters.  
%Names can use letters, numbers, and underscores,
%but the first character must be a letter.
%
%Example:  
% Paste the code below into a new .m file and save the new file
% with name "example_function.m"    
%function [output1,output2]=example_function(a,b,c)
%output1=a+b;
%output2=b*c;
[a,b] = example_function(1,2,3) %try this call after creating the .m file

% Arrays
% An array is a means of storing a sequence of numbers.
a=[1 2 3 4 5]	
%OR	
a=[1,2,3,4,5]
whos
a(2)
a(3)
b=[2 3 5 7 11 13 17 19];
b(4)
% 
% You can change entries in an array using the array editor.
% 
% Operations with arrays
5*a
a/2
a(6)=6
a(7)=7
a(8)=8
%a*b % taboo, since default multiplication is matrix multiplication
a.*b  %use dot to designate element-wise actions
a.^2


% Logical Variables
% Matlab uses logical variables or arrays 
% to denote a true or false statement using the numbers 1 and 0.
% 
% Example:
a = 40
a > 50
a < 10
a >= 39
% 
% Relational Operators:
% < 	less than
% <= 	less than or equal to
% > 	greater than
% >=	greater than or equal to
% = = 	equal to
% ~ = 	not equal to
% NOTE: when checking if a value is equal to another value, 
% you MUST use = =, NOT = !
% 
% Example:
a = [1 2 3 4 5]
b = [2 2 1 7 9]
a > b
a < b
a == b
% 
% Logical Operators
% &	 and
% |	 or
% ~	 not
% 
% Example:
x = 1:9
y = x<4 | x>8
p = x.*(x<4 | x>8)
q = ~(x<4)
q = x.*~(x<4)
z = x<4 & x>8
%
%
% Control Flow Statements:
% For loops:
for j=1:10
    j
end
% 
for j=1:10
    5*j
end
% 
for j=1:10
    fprintf('hello guest %d\n', j)
end
% how many primes from 1 to 20???
number=0;
for j=1:20
    number=number+isprime(j);
end
number
% 
for j=0:5
    for k=0:9
        fprintf('%d%d\n',j,k)
    end
end
% This generates a 1x5 vector A 
for j=1:5
    A(j)=j;
end
A
% This generates a 5x5 matrix M.  Will will return to this idea.
for j=1:5
    for k=1:5
        M(j,k)=j*k;
    end
end
M
% 
% If statements:
x=15;
if x<25
    disp 'x is less than 25'
end
% 
% 
% 
x=52;
if x<25
    disp 'x is less than 25'
end
% 
x=50;
if x<50
    disp 'x is less than 50'
elseif x>50
    disp 'x is greater than 50'
else
    disp 'x is 50'
end
% 
% % run with x=40, then x=22.
x=40;
if x<25
    disp 'x is less than 25'
elseif x<50
    disp 'x is less than 50'
else
    disp 'x is greater than or equal to 50'
end
% 
% %%Only ONE statement is executed.
% 
for j=1:20
    if isprime(j) == 1
        j
    end
end
% 
% While loops:
j=0;
while j<10
    j=j+3;
end
j
% 
numsteps=0;
tolerance=1;
while tolerance>0.01
   %do stuff
   tolerance=tolerance*1/2;
   numsteps=numsteps+1;
end
% Some useful commands:
% Recall 1:10 results in 1 2 3 4 5 6 7 8 9 10.  
% The command 1:2:10 results in 1 3 5 7 9.
% It�s useful in for loops to sometimes use something like 10:-1:1, 
% which gives 10,9,8,�,1.
% The command rand(1,10) gives an array of random numbers between 0 and 10.
% To create an empty array called �myarray,� type
myarray = []
% 
% Machine Epsilon:
% Computers can differentiate between numbers up to a certain precision.  
% The command �eps� gives the smallest number such that, if added to 1, 
% will give a number that the computer recognizes as different from 1.
% 

%Matrices 
A = zeros(7,5);
for i = 1:size(A,1)
    for j = 1:size(A,2)
        A(i,j) = i*j;
    end
end
A
v = ones(5,1);
w=2*ones(7,1);
A*v
w'*A % ' means transpose
A(:,3) %show the 3rd column
A(2,:) %show the 2nd row

%Next class:  Digital Photography Processing 
% We will continue talking about matrices, in the context of digital photos.


